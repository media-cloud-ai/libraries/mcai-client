use crate::{
  error::{Error, Result},
  request::Login,
  response::access_token::AccessToken,
  Action,
};
use reqwest::{header::CONTENT_TYPE, Client as ReqwestClient, Response};

#[derive(Clone, Debug)]
pub struct Client {
  pub client: ReqwestClient,
  pub token: Option<String>,
  pub hostname: String,
  pub password: String,
  pub username: String,
}

impl Client {
  pub fn new(hostname: String, username: String, password: String) -> Self {
    let client = ReqwestClient::new();
    Client {
      client,
      token: None,
      hostname,
      password,
      username,
    }
  }

  pub fn get_session_url(&self) -> String {
    self.build_url("/api/sessions")
  }

  pub fn build_url(&self, endpoint: &str) -> String {
    format!("{}{}", self.hostname, endpoint)
  }

  async fn get_authentication_token(&mut self) -> Result<String> {
    if let Some(token) = &self.token {
      return Ok(token.clone());
    }
    let login_body = Login::new(self.username.as_str(), self.password.as_str()).to_string();

    let url = self.get_session_url();

    let response = self
      .client
      .post(url)
      .header(CONTENT_TYPE, "application/json")
      .body(login_body)
      .send()
      .await?;

    if !response.status().is_success() {
      return Err(Error::Runtime("Request failed".to_string()));
    }

    let token_response: AccessToken = response.json().await?;
    self.token = Some(token_response.access_token.clone());
    Ok(token_response.access_token)
  }

  pub async fn request(&mut self, action: Action) -> Result<Response> {
    let url = self.build_url(&action.get_url());

    let request = self
      .client
      .request(action.get_method(), url)
      .bearer_auth(self.get_authentication_token().await?);

    let request = if let Some(body) = action.get_body() {
      request
        .body(body)
        .header(CONTENT_TYPE, action.get_content_type().unwrap())
    } else {
      request
    };
    request.send().await.map_err(Error::Reqwest)
  }

  pub async fn request_and_parse<T>(&mut self, action: Action) -> Result<T>
  where
    T: for<'de> serde::Deserialize<'de>,
  {
    let response = self.request(action).await?;
    let result = response.json::<T>().await?;
    Ok(result)
  }
}
