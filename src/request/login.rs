use crate::request::session::Session;
use serde::Serialize;

#[derive(Debug, Default, Serialize)]
pub struct Login {
  session: Session,
}

impl Login {
  pub fn new(username: &str, password: &str) -> Self {
    Login {
      session: Session {
        email: username.to_string(),
        password: password.to_string(),
      },
    }
  }
}

impl ToString for Login {
  fn to_string(&self) -> String {
    serde_json::to_string(&self).expect("unable to serialize Login")
  }
}
