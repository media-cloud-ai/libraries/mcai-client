use std::fmt::{self, Display, Formatter};

#[derive(Debug)]
pub enum Error {
  Reqwest(reqwest::Error),
  SerdeJson(serde_json::Error),
  Runtime(String),
}

pub type Result<T> = std::result::Result<T, Error>;

impl std::error::Error for Error {}

impl From<reqwest::Error> for Error {
  fn from(error: reqwest::Error) -> Self {
    Error::Reqwest(error)
  }
}

impl From<serde_json::Error> for Error {
  fn from(error: serde_json::Error) -> Self {
    Error::SerdeJson(error)
  }
}

impl Display for Error {
  fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
    match self {
      Error::Runtime(string) => write!(f, "RUNTIME : {}", string),
      Error::Reqwest(error) => write!(f, "HTTP request: {}", error),
      Error::SerdeJson(error) => write!(f, "JSON: {}", error),
    }
  }
}
