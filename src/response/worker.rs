use mcai_models::Worker;
use serde::{Deserialize, Serialize};

#[derive(Deserialize, Serialize)]
pub struct WorkerList {
  pub data: Vec<Worker>,
  pub total: usize,
}

#[derive(Deserialize, Serialize)]
pub struct WorkerShow {
  #[serde(flatten)]
  pub data: Worker,
}
