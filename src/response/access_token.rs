use serde::{Deserialize, Serialize};

#[derive(Debug, Default, Serialize, Deserialize)]
pub struct AccessToken {
  pub access_token: String,
  pub user: User,
}

#[derive(Debug, Default, Serialize, Deserialize)]
pub struct User {
  pub email: String,
  pub id: u32,
  pub roles: Vec<String>,
}
