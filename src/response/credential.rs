use mcai_models::Credential;
use serde::{Deserialize, Serialize};

#[derive(Deserialize)]
pub struct CredentialList {
  pub data: Vec<Credential>,
  pub total: usize,
}

#[derive(Deserialize, Serialize)]
pub struct CredentialShow {
  pub data: Credential,
}
