use mcai_models::{WorkflowDuration, WorkflowInstance};
use serde::{Deserialize, Serialize};

#[derive(Deserialize)]
pub struct WorkflowList {
  pub data: Vec<WorkflowInstance>,
  pub total: usize,
}

#[derive(Deserialize, Serialize)]
pub struct WorkflowShow {
  pub data: WorkflowInstance,
}

#[derive(Debug, Deserialize, Serialize)]
pub struct WorkflowDurationShow {
  pub data: Vec<WorkflowDuration>,
}
