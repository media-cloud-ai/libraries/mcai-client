use mcai_models::User;
use serde::{Deserialize, Serialize};

#[derive(Deserialize)]
pub struct UserList {
  pub data: Vec<User>,
  pub total: usize,
}

#[derive(Deserialize, Serialize)]
pub struct UserShow {
  pub data: User,
}
