pub struct Pagination {
  pub page: u64,
  pub size: u64,
}
