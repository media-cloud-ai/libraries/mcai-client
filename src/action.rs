use crate::{error::Result, Pagination};
use chrono::{DateTime, Utc};
use mcai_models::CredentialContent;
use reqwest::Method;
use serde::Serialize;

pub enum Action {
  AbortWorkflow {
    identifier: i64,
  },
  AddCredential(CredentialContent),
  DeleteCredential {
    identifier: i64,
  },
  DeleteWorkflow {
    identifier: i64,
  },
  GetWorkflowDuration {
    identifier: i64,
  },
  ListCredential {
    pagination: Pagination,
  },
  ListUser {
    pagination: Pagination,
  },
  ListWorker {
    identifier: Option<String>,
    job_id: Option<String>,
    pagination: Pagination,
  },
  ListWorkflow {
    after_date: Option<DateTime<Utc>>,
    before_date: Option<DateTime<Utc>>,
    name: Option<String>,
    states: Vec<String>,
    pagination: Pagination,
  },
  ShowCredential {
    identifier: String,
  },
  ShowUser {
    identifier: i64,
  },
  ShowWorker {
    identifier: String,
  },
  ShowWorkflow {
    identifier: i64,
    mode: ShowWorkflowMode,
  },
  StopWorker {
    identifier: String,
  },
  StopWorkflow {
    identifier: i64,
  },
  WorkerJobConsumptionResume {
    identifier: String,
  },
  WorkerJobConsumptionStop {
    identifier: String,
  },
}

pub enum ShowWorkflowMode {
  Full,
  Simple,
}

impl Action {
  pub fn get_url(&self) -> String {
    let endpoint = match self {
      Action::AbortWorkflow { identifier }
      | Action::DeleteWorkflow { identifier }
      | Action::StopWorkflow { identifier } => {
        format!("/step_flow/workflows/{}/events", identifier)
      }
      Action::AddCredential(_) => "/credentials".to_string(),
      Action::DeleteCredential { identifier } => {
        format!("/credentials/{}", identifier)
      }
      Action::GetWorkflowDuration { identifier } => {
        format!("/step_flow/durations/workflows?workflow_id={}", identifier)
      }
      Action::ListCredential { pagination } => {
        format!(
          "/credentials?page={}&size={}",
          pagination.page, pagination.size
        )
      }
      Action::ListUser { pagination } => {
        format!("/users?page={}&size={}", pagination.page, pagination.size)
      }

      Action::ListWorker {
        identifier,
        job_id,
        pagination,
      } => {
        let base_url = "/step_flow/workers".to_string();
        let mut url_options = vec![
          format!("page={}", pagination.page),
          format!("size={}", pagination.size),
        ];
        if let Some(single_instance_id) = identifier {
          url_options.push(format!("instance_id={}", single_instance_id))
        }

        if let Some(single_job_id) = job_id {
          url_options.push(format!("job_id={}", single_job_id))
        }
        format!("{}?{}", base_url, url_options.join("&"))
      }

      Action::ListWorkflow {
        name,
        after_date,
        before_date,
        states,
        pagination,
      } => {
        let base_url = "/step_flow/workflows".to_string();
        let mut url_options = vec![
          format!("page={}", pagination.page),
          format!("size={}", pagination.size),
        ];
        if let Some(id) = name {
          url_options.push(format!("workflow_ids[]={}", id))
        }
        if let Some(date) = after_date {
          url_options.push(format!("after_date={}", date.format("%Y-%m-%dT%H:%M:%S")))
        }
        if let Some(date) = before_date {
          url_options.push(format!("before_date={}", date.format("%Y-%m-%dT%H:%M:%S")))
        }
        for state in states {
          url_options.push(format!("states[]={}", state))
        }
        format!("{}?{}", base_url, url_options.join("&"))
      }

      Action::ShowCredential { identifier } => format!("/credentials/{}", identifier),
      Action::ShowUser { identifier } => format!("/users/{}", identifier),
      Action::ShowWorkflow { identifier, mode } => match mode {
        ShowWorkflowMode::Full => format!("/step_flow/workflows/{}", identifier),
        ShowWorkflowMode::Simple => format!("/step_flow/workflows/{}?mode=simple", identifier),
      },
      Action::ShowWorker { identifier }
      | Action::StopWorker { identifier }
      | Action::WorkerJobConsumptionResume { identifier }
      | Action::WorkerJobConsumptionStop { identifier } => {
        format!("/step_flow/workers/{}", identifier)
      }
    };
    format!("/api{}", endpoint)
  }

  pub fn get_body(&self) -> Option<String> {
    match self {
      Action::AbortWorkflow { .. } => WorkflowAction::Abort.as_body().ok(),
      Action::AddCredential(credential) => serde_json::to_string(credential).ok(),
      Action::DeleteWorkflow { .. } => WorkflowAction::Delete.as_body().ok(),
      Action::StopWorker { .. } => WorkerAction::StopWorker.as_body().ok(),
      Action::WorkerJobConsumptionResume { .. } => WorkerAction::ResumeConsumingJobs.as_body().ok(),
      Action::WorkerJobConsumptionStop { .. } => WorkerAction::StopConsumingJobs.as_body().ok(),
      Action::StopWorkflow { .. } => WorkflowAction::Stop.as_body().ok(),
      _ => None,
    }
  }

  pub fn get_method(&self) -> Method {
    match self {
      Action::AbortWorkflow { .. }
      | Action::AddCredential { .. }
      | Action::DeleteWorkflow { .. }
      | Action::StopWorkflow { .. } => Method::POST,
      Action::DeleteCredential { .. } => Method::DELETE,
      Action::GetWorkflowDuration { .. }
      | Action::ListCredential { .. }
      | Action::ListUser { .. }
      | Action::ListWorker { .. }
      | Action::ListWorkflow { .. }
      | Action::ShowCredential { .. }
      | Action::ShowUser { .. }
      | Action::ShowWorker { .. }
      | Action::ShowWorkflow { .. } => Method::GET,
      Action::StopWorker { .. }
      | Action::WorkerJobConsumptionResume { .. }
      | Action::WorkerJobConsumptionStop { .. } => Method::PUT,
    }
  }

  pub fn get_content_type(&self) -> Option<String> {
    match self {
      Action::AbortWorkflow { .. }
      | Action::AddCredential { .. }
      | Action::DeleteWorkflow { .. }
      | Action::StopWorker { .. }
      | Action::StopWorkflow { .. }
      | Action::WorkerJobConsumptionResume { .. }
      | Action::WorkerJobConsumptionStop { .. } => Some("application/json".to_string()),
      _ => None,
    }
  }
}

#[derive(Serialize)]
#[serde(tag = "type", rename_all = "snake_case")]
pub enum WorkerAction {
  ResumeConsumingJobs,
  StopConsumingJobs,
  StopWorker,
}

impl WorkerAction {
  pub(crate) fn as_body(&self) -> Result<String> {
    Ok(serde_json::to_string(self)?)
  }
}

#[derive(Serialize)]
#[serde(tag = "event", rename_all = "snake_case")]
pub enum WorkflowAction {
  Abort,
  Delete,
  Stop,
}

impl WorkflowAction {
  pub(crate) fn as_body(&self) -> Result<String> {
    Ok(serde_json::to_string(self)?)
  }
}
